#include <v2lwip.h>
#include <fduserdata.h>
#include <errno.h>

FDUSERDATA *stacks;

lwip_stack *v2lwip_addstack() {
    return bsd2lwip_newstack(NULL,NULL);
}

void v2lwip_delstack(lwip_stack *stack) {
    bsd2lwip_delstack(stack);
}

int v2lwip_msocket(lwip_stack *stack, int domain, int type, int protocol) {
    int ret = bsd2lwip_socket(stack,domain,type,protocol);
    if (ret < 0)
        return -1;
    else
    {
        lwip_stack **st = fduserdata_new(stacks,ret,lwip_stack*);
        if (st == NULL)
        {
            bsd2lwip_close(stack,ret);
            return errno = ENOMEM,-1;
        }
        else
        {
            *st = stack;
            fduserdata_put(st);
            return ret;
        }
    }
}

int v2lwip_close(int s) {
    lwip_stack **stack = fduserdata_get(stacks,s);
    if (stack){
        bsd2lwip_close(*stack,s);
        fduserdata_del(stack);
        return 0;
    }
    else
        return errno=EBADF,-1;
}

int v2lwip_bind(int s, const struct sockaddr * name, socklen_t namelen) {
    lwip_stack **stack = fduserdata_get(stacks,s);
    int ret = -1;
    if (stack){
        ret = bsd2lwip_bind(*stack, s, name, namelen);
        fduserdata_put(stack);
    }

    return ret;
}

int v2lwip_listen(int s, int backlog) {
    lwip_stack **stack = fduserdata_get(stacks,s);
    int ret = -1;
    if (stack){
        ret = bsd2lwip_listen(*stack, s, backlog);
        fduserdata_put(stack);
    }

    return ret;
}

int v2lwip_accept(int s, struct sockaddr * addr, socklen_t * addrlen) {
    lwip_stack **stack = fduserdata_get(stacks,s);
    int ret = -1;
    if (stack){
        ret = bsd2lwip_accept4(*stack, s, addr, addrlen, 0);
        fduserdata_put(stack);
    }

    return ret;
}

ssize_t v2lwip_recv(int sockfd, void * buf, size_t len, int flags) {
    lwip_stack **stack = fduserdata_get(stacks,sockfd);
    int ret = -1;
    if (stack){
        ret = bsd2lwip_recv(*stack, sockfd, buf, len, flags);
        fduserdata_put(stack);
    }

    return ret;
}

ssize_t v2lwip_send(int sockfd, const void * buf, size_t len, int flags) {
    lwip_stack **stack = fduserdata_get(stacks,sockfd);
    int ret = -1;
    if (stack){
        ret = bsd2lwip_send(*stack, sockfd, buf, len, flags);
        fduserdata_put(stack);
    }

    return ret;
}

void destroy_stack(int fd, void *stack, void *arg) {
    bsd2lwip_delstack((lwip_stack *)stack);
}

void __attribute__((constructor)) init(){
    // TODO Check for null return
    stacks = fduserdata_create(NUM_STACKS);
    fduserdata_destroy_cb(stacks,destroy_stack,NULL);
}

void __attribute__((destructor)) fini(){
    fduserdata_destroy(stacks);
}
