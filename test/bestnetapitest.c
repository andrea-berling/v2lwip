#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h> /* superset of previous */
#include <poll.h>
#include <nlinline+.h>
#include <v2lwip.h>

#define SERV_PORT 3456
#define MAXCONN 4

NLINLINE_LIBMULTI(v2lwip_)

	ssize_t str_echo(int i, int fd) {
		char buf[1025];
		ssize_t len;
		if ((len = v2lwip_recv(fd, buf, 1024, 0)) > 0) {
			v2lwip_send(fd, buf, len, 0);
			buf[len] = 0;
			printf("echo %d -> %s\n", i, buf);
		} 
	}

void server(lwip_stack *stack) {
	struct sockaddr_in  cliaddr, servaddr;
	struct pollfd pfd[MAXCONN + 1] = {[0 ... MAXCONN] = {-1, POLLIN, 0}};
	int rv;

	pfd[0].fd = v2lwip_msocket(stack, AF_INET, SOCK_STREAM, 0);
	printf("v2lwip_socket %d\n", pfd[0].fd);

	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family      = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port        = htons(SERV_PORT);

	rv = v2lwip_bind(pfd[0].fd, (struct sockaddr *) &servaddr, sizeof(servaddr));
	printf("v2lwip_bind %d\n", rv);

	rv = v2lwip_listen(pfd[0].fd, 5);
	printf("v2lwip_listen %d\n", rv);

	for ( ; ; ) {
		int i;
		int events = poll(pfd, MAXCONN + 1, -1);
		if (events < 0) break;
		if (pfd[0].revents & POLLIN) {
			int         connfd;
			struct sockaddr_in  cliaddr;
			socklen_t     clilen = sizeof(cliaddr);

			connfd = v2lwip_accept(pfd[0].fd, (struct sockaddr *) &cliaddr, &clilen);
			for (i = 1; i <= MAXCONN; i++) {
				if (pfd[i].fd < 0) {
					pfd[i].fd = connfd;
					break;
				}
			}
			if (i > MAXCONN) {
				v2lwip_close(connfd);
				i = -1;
			}
			printf("v2lwip_accept %d -> %d\n", connfd, i);
			events--;
		}
		for (i = 1; i <= MAXCONN && events > 0; i++) {
			if (pfd[i].revents & POLLIN) {
				if (str_echo(i, pfd[i].fd) <= 0) {
					v2lwip_close(pfd[i].fd);
					pfd[i].fd = -1;
					printf("close %d\n", i);
				}
				events--;
			}
		}
	}
}

int main(int argc, char *argv[]) {
	int rv;
	uint8_t ipv4addr[] = {192,168,250,42};
	uint8_t ipv4gw[] = {192,168,250,1};
	uint8_t ipv6addr[16] = {0x20, 0x01, 0x07, 0x60, [15] = 0x02};
	uint8_t ipv6gw[16] = {0x20, 0x01, 0x07, 0x60, [15] = 0x01};

	lwip_stack *stack = v2lwip_addstack();

	if (v2lwip_iplink_add(stack, "vd0", 0, "vde", argv[1]) < 0)
		perror("link add");

	int ifindex = v2lwip_if_nametoindex(stack, "vd0");
	if (ifindex > 0)
		printf("%d\n", ifindex);
	else {
		perror("nametoindex");
		return 1;
	}

	if (v2lwip_linksetupdown(stack, ifindex, 1) < 0)
		perror("link up");
	if (v2lwip_ipaddr_add(stack, AF_INET, ipv4addr, 24, ifindex) < 0)
		perror("addr ipv4");
	if (v2lwip_iproute_add(stack, AF_INET, NULL, 0, ipv4gw) < 0)
		perror("addr ipv6");
	if (v2lwip_ipaddr_add(stack, AF_INET6, ipv6addr, 64, ifindex) < 0)
		perror("route ipv4");
	if (v2lwip_iproute_add(stack, AF_INET6, NULL, 0, ipv6gw) < 0)
		perror("route ipv6");

	/* use the stack */
	server(stack);

	v2lwip_delstack(stack);
}
