#ifndef V2LWIP_H
#define V2LWIP_H

#include <bsd2lwip.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>

#define NUM_STACKS 10

typedef struct stack_data lwip_stack;

lwip_stack *v2lwip_addstack();
void v2lwip_delstack(lwip_stack *stack);

int v2lwip_msocket(lwip_stack *stack, int domain, int type, int protocol);
int v2lwip_bind(int s, const struct sockaddr *name, socklen_t namelen);
int v2lwip_listen(int s, int backlog);
int v2lwip_accept(int s, struct sockaddr *addr, socklen_t *addrlen);
int v2lwip_close(int s);
ssize_t v2lwip_recv(int sockfd, void *buf, size_t len, int flags);
ssize_t v2lwip_send(int sockfd, const void *buf, size_t len, int flags);

#endif /* V2LWIP_H */
