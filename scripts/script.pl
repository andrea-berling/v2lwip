#!/usr/bin/env perl

$template="%s %s_%s(%s) {
    lwip_stack **stack = fduserdata_get(stacks,%s);
    int ret = -1;
    if (stack){
        ret = bsd2lwip_%s(*stack, %s);
        fduserdata_put(stack);
    }

    return ret;
}\n\n";

sub print_fun_dec{
    $ARG = $_[0];
    if (@matches = $ARG =~ /[a-zA-Z0-9_]+/g)
    {
        my $fun_name = $matches[1];
        my $return_type = $matches[0];
        my $dec_args = $1 if $ARG =~ /[a-zA-Z0-9_]+ [a-zA-Z0-9]+ (.*)/;
        my $call_args="";
        my $match = $dec_args;
        while($match ne "")
        {
            if ($match =~ /([^,]+),([^ ]+) ?(.*)/)
            {
                $match = $3;
            }
            else
            {
                $match = "";
            }
            $call_args .= $2.", ";
        }
        #@matches = $dec_args =~ /"[^"]+",[a-zA-Z_]+/g;
        #foreach my $match (@matches)
        #{
        #    $match =~ /"([^"]+)",([a-zA-Z_]+)/g;
        #    #print "1:",$1;
        #    #print "\n";
        #    #print "2:",$2;
        #    #print "\n";
        #}
        $call_args =~ s/, $//;
        #$dec_args =~ s/ "/, /g;
        #$dec_args =~ s/",/ /g;
        $dec_args =~ s/,([a-zA-Z0-9]+)/ \1,/g;
        $dec_args =~ s/,$//;
        $first_arg = $1 if $call_args =~ /([^,]+),/;
        printf $template,$return_type,$ENV{'PREFIX'},$fun_name,$dec_args,$first_arg,$fun_name,$call_args;
    }
    else
    {
        print "No match\n"
    }
}

#print_fun_dec('int bind "int",sockfd "const struct sockaddr *",addr "socklen_t",addrlen');

use strict;
use warnings;

my $file = 'mod_funs';
open my $info, $file or die "Could not open $file: $!";

while( my $line = <$info>)  {
    print_fun_dec($line);
}

close $info;
